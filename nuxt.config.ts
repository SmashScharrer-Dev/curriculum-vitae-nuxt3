import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
    buildModules: [
        '@nuxtjs/tailwindcss'
    ],
    // CSS Config
    css: [
        '@/assets/css/app.css',
        '@/assets/css/modern-normalize.css'
    ]
})
