<h1 align="center">Welcome to my curriculum vitae 👋</h1>
<p>
  <img src="https://img.shields.io/badge/npm-%3E%3D8.5.5-blue.svg" />
  <img src="https://img.shields.io/badge/node-%3E%3D16.15.0-blue.svg" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> My curriculum vitae, developed with the Nuxt JS framework.

### 🏠 [Homepage](https://nathan-guirado-patrico-cv.vercel.app/)

## Prerequisites

- npm >=8.5.5
- node >=16.15.0

## Install

```sh
npm install
```

## Author

👤 **Nathan GUIRADO-PATRICO**


## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitlab.com/SmashScharrer-Dev/curriculum-vitae-nuxt3/-/issues). You can also take a look at the [contributing guide](https://gitlab.com/SmashScharrer-Dev/curriculum-vitae-nuxt3/blob/master/CONTRIBUTING.md).

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_